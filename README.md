# Django Boilerplate

Simple django boilerplate project with configured setting to read from env variables and some useful packages installed.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

```bash
# First create a virtual env
python3 -m venv venv

# Activate the virtual env based on your shell
source venv/bin/activate
source venv/bin/activate.fish

# Install requirements
pip install -r requirements.txt

# Create env config file
cp .env.tmp .env
# Then fix env variables in the .env file based on your project

python manage.py makemigrations
python manage.py migrate

python manage.py runserver
```

The development server will be running at http://127.0.0.1:8000/
