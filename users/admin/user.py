from hijack_admin.admin import HijackUserAdminMixin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from users.models import User


class UserAdmin(UserAdmin, HijackUserAdminMixin):
    list_display = (
        'username',
        'email',
        'first_name',
        'last_name',
        'is_staff',
        'hijack_field',  # Hijack button
    )


admin.site.register(User, UserAdmin)
